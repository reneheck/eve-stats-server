package wtf.heck.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by cerias on 17.01.15.
 */
@ApplicationPath("v1")
public class JAXRSApplication extends Application {
}
