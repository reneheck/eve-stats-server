package wtf.heck.rest.endpoint;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Created by cerias on 17.01.15.
 */
@Path("item")
public class Items {

    @Path("count")
    @GET
    public Response count(){

        return Response.ok().build();
    }
}
