package wtf.heck.rest.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogram;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.percentiles.PercentileRanks;
import org.elasticsearch.search.aggregations.metrics.stats.Stats;
import org.elasticsearch.search.aggregations.metrics.stats.extended.ExtendedStats;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.rest.query.LossQuery;
import wtf.heck.rest.query.OverviewQuery;
import wtf.heck.zkillboard.Zkilloard;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by cerias on 05.02.15.
 */
@Path("killmail")
public class Killmail {

    @Inject
    ElasticSearchClient esc;

    ObjectMapper mapper = new ObjectMapper();
    @Path("loss/{type}/{id}")
    @POST @Consumes("application/json")
    public Response stat(@PathParam("type")String type,@PathParam("id")String id,final LossQuery query) throws IOException {

        SearchRequestBuilder request = esc.getClient().prepareSearch(esc.getINDEX());
        request.setTypes("killmail");

        /***
         * search
         */

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        boolQueryBuilder.must(QueryBuilders.matchQuery("victim."+type+"Name.untouched",id));



        if(query.from!=null&&query.to!=null){
            boolQueryBuilder.must(QueryBuilders.rangeQuery("killTime").from(query.from).to(query.to));
        }
        /***
         * aggs
         */
        request.addAggregation(

                AggregationBuilders.extendedStats("stats").field("isk.zTotalValue")

        );
        Double[] percentiels = new Double[8];

            request.addAggregation(
                    AggregationBuilders.dateHistogram("histogram").field("killTime").interval(DateHistogram.Interval.MONTH).order(Histogram.Order.KEY_DESC)
                            .subAggregation(AggregationBuilders.extendedStats("stats").field("isk.zTotalValue"))
                            .subAggregation(AggregationBuilders.percentileRanks("perRank").field("isk.zTotalValue")
                                    .percentiles(0, 1000000,50000000, 250000000, 500000000, 1000000000)));



        request.setQuery(boolQueryBuilder);



        SearchResponse response = request.execute().actionGet();


        Map<String,Object> resp = new HashMap<>();
        if(response.getHits().getTotalHits()>0){
            Zkilloard kill = mapper.readValue(response.getHits().getAt(0).getSourceAsString(), Zkilloard.class);
                    resp.put("name", kill.getName(type));
        }
        ExtendedStats stats = response.getAggregations().get("stats");

        if(query.interval>100000){
            Histogram histogram = response.getAggregations().get("histogram");
            ArrayList<Map<String,Object>> history = new ArrayList<>();
            for(Histogram.Bucket h : histogram.getBuckets()){
                Stats stats1 = h.getAggregations().get("stats");
                PercentileRanks percentileRanks = h.getAggregations().get("perRank");
                Map<String,Object> hist = new HashMap<>();
                hist.put("time",h.getKeyAsNumber());
                hist.put("stats",stats1);
                Map<Double,Double> perMap = new HashMap<>();
                percentileRanks.forEach(it -> perMap.put(it.getValue(),it.getPercent()));
                hist.put("per_rank",perMap);
                history.add(hist);
            }
            resp.put("histogramData",history);
        }



        resp.put("stats",stats);

        return Response.ok(mapper.writeValueAsString(resp), MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("shipsTypes/top/{ranks}/type/{type}/id/{id}")
    @POST
    @Consumes("application/json")
    public Response itemTop(@PathParam("ranks")int ranks,final LossQuery query,@PathParam("type")String type,@PathParam("id")String id) throws JsonProcessingException {

        SearchRequestBuilder request = esc.getClient().prepareSearch(esc.getINDEX());
        request.setTypes("killmail");
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();

        boolQueryBuilder.must(QueryBuilders.matchQuery("victim."+type+"Name.untouched",id));

        if(query.from!=null&&query.to!=null){
            boolQueryBuilder.must(QueryBuilders.rangeQuery("killTime").from(query.from).to(query.to));
        }

        request.setQuery(boolQueryBuilder);
        request.addAggregation(
                AggregationBuilders.terms("destroyed").field("victim.shipType.name.untouched").size(ranks)
        );

        request.addAggregation(
                AggregationBuilders.terms("killing").field("attackers.shipType.name.untouched").size(ranks)
        );


        SearchResponse searchResponse = request.execute().actionGet();

        Map<String,Object> retMap = new HashMap<>();

        Terms destroyed = searchResponse.getAggregations().get("destroyed");

        retMap.put("destroyed",termsToMap(destroyed));
        retMap.put("attacking",termsToMap(searchResponse.getAggregations().get("killing")));


        return Response.ok(mapper.writeValueAsString(retMap)).build();
    }

    private ArrayList<Map<String,Object>> termsToMap(Terms terms){
        ArrayList<Map<String,Object>> destroyedList = new ArrayList<>();
        for(Terms.Bucket bucket:terms.getBuckets()){
            Map<String,Object> dest = new HashMap<>();
            dest.put("name",bucket.getKey());
            dest.put("value",bucket.getDocCount());

            destroyedList.add(dest);
        }

        return destroyedList;
    }
}
