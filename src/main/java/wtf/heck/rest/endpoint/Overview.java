package wtf.heck.rest.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.stats.Stats;
import org.elasticsearch.search.aggregations.metrics.stats.StatsAggegator;
import wtf.heck.elasticsearch.AggregationService;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.rest.query.OverviewQuery;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.concurrent.SynchronousQueue;

/**
 * Created by cerias on 17.01.15.
 */
@Path("overview")
public class Overview {

    @Inject
    ElasticSearchClient esc;

    @Inject
    AggregationService aggService;

    ObjectMapper mapper = new ObjectMapper();

    @Path("killmail/count/")
    @GET
    public Response kmCount() {

        CountRequestBuilder request = esc.getClient().prepareCount(esc.getINDEX());
        request.setTypes("killmail");
        CountResponse response = request.execute().actionGet();

        return Response.ok(response.getCount() + "", MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("killmail/count/{type}/{id}")
    @GET
    public Response kmCount(@PathParam("type") String type, @PathParam("id") String id) {

        CountRequestBuilder request = esc.getClient().prepareCount(esc.getINDEX());
        request.setTypes("killmail");
        request.setQuery(QueryBuilders.matchQuery(type, id));
        CountResponse response = request.execute().actionGet();


        return Response.ok(response.getCount() + "", MediaType.TEXT_PLAIN_TYPE).build();
    }

    @Path("ship/race/{type}")
    @POST
    @Consumes("application/json")
    public Response ships(@PathParam("type")String type,final OverviewQuery query) throws Exception {
        SearchRequestBuilder request = esc.getClient().prepareSearch(esc.getINDEX());
        request.setTypes("killmail");
        request.setQuery(QueryBuilders.rangeQuery("killTime").from(query.from).to(query.to));

        aggService.raceMarketAggregations(type,"shipType").forEach(it->request.addAggregation(it));

        SearchResponse searchResponse = request.execute().actionGet();

        return Response.ok(mapper.writeValueAsString(searchResponse.getAggregations().asList())).build();

    }


    @Path("shipsTypes/top/{ranks}")
    @POST
    @Consumes("application/json")
    public Response itemTop(@PathParam("ranks")int ranks,final OverviewQuery query) throws JsonProcessingException {

        SearchRequestBuilder request = esc.getClient().prepareSearch(esc.getINDEX());
        request.setTypes("killmail");
        request.setQuery(QueryBuilders.rangeQuery("killTime").from(query.from).to(query.to));

        request.addAggregation(
                AggregationBuilders.terms("destroyed").field("victim.shipType.name.untouched").size(ranks)
        );

        request.addAggregation(
                AggregationBuilders.terms("killing").field("attackers.shipType.name.untouched").size(ranks)
        );

        SearchResponse searchResponse = request.execute().actionGet();

        Map<String,Object> retMap = new HashMap<>();

        Terms destroyed = searchResponse.getAggregations().get("destroyed");

        retMap.put("destroyed",termsToMap(destroyed));
        retMap.put("attacking",termsToMap(searchResponse.getAggregations().get("killing")));


        return Response.ok(mapper.writeValueAsString(retMap)).build();
    }


    private ArrayList<Map<String,Object>> termsToMap(Terms terms){
        ArrayList<Map<String,Object>> destroyedList = new ArrayList<>();
        for(Terms.Bucket bucket:terms.getBuckets()){
            Map<String,Object> dest = new HashMap<>();
            dest.put("name",bucket.getKey());
            dest.put("value",bucket.getDocCount());

            destroyedList.add(dest);
        }

        return destroyedList;
    }

    @Path("killmail/top/{size}/type/{type}")
    @POST
    @Consumes("application/json")
    public Response top100(@PathParam("type")String type,@PathParam("size")int size,final OverviewQuery overview) throws JsonProcessingException {

        System.out.println(overview.name);
        SearchRequestBuilder request = esc.getClient().prepareSearch(esc.getINDEX());
        request.setTypes("killmail");

        if(overview.getName()!=null){
            request.setQuery(QueryBuilders.wildcardQuery("victim."+type+"Name","*"+overview.getName()+"*"));
//            request.setQuery(QueryBuilders.fuzzyQuery("victim."+type+"Name",overview.getName()));
        }
        request.addAggregation(
                AggregationBuilders.terms("alliance").field("victim."+type+"Name.untouched").size(size)
                        .subAggregation(AggregationBuilders.stats("stats").field("isk.zTotalValue")
                        ).subAggregation(AggregationBuilders.max("latestKill").field("killTime"))
        );

        SearchResponse response = request.execute().actionGet();



        Terms terms = response.getAggregations().get("alliance");
        ArrayList<Map<String,Object>> stats = new ArrayList<>();
        for(Terms.Bucket bucket :terms.getBuckets()){
            Map<String,Object> m = new HashMap<>();
            m.put("stats",bucket.getAggregations().get("stats"));
            m.put("latestKill",bucket.getAggregations().get("latestKill"));

            m.put("name",bucket.getKey());
            stats.add(m);
        }


        return Response.ok(mapper.writeValueAsString(stats),MediaType.APPLICATION_JSON_TYPE).build();
    }


}
