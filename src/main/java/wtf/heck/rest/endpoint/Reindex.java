package wtf.heck.rest.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.count.CountRequestBuilder;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.delete.DeleteRequestBuilder;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import wtf.heck.elasticsearch.ElasticSearchClient;
import wtf.heck.zkillboard.Zkilloard;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by cerias on 28.01.15.
 */
@Path("es")
public class Reindex {
    @Inject
    ElasticSearchClient esc;
    ObjectMapper mapper = new ObjectMapper();

    @Path("reIndex")
    @GET
    public Response reindex() throws IOException {
        CountRequestBuilder request = esc.getClient().prepareCount(esc.getINDEX());
        request.setTypes("kills");
        CountResponse response = request.execute().actionGet();

        SearchRequestBuilder rein = esc.getClient().prepareSearch(esc.getINDEX());
        rein.setTypes("kills");
        rein.setSize(1000);
        for(int i=0;i<response.getCount()/1000;i++){
            rein.setFrom(i*1000);
            ArrayList<Zkilloard> kills = new ArrayList<>();
            for(SearchHit hit :rein.execute().actionGet().getHits().getHits()){
                kills.add(mapper.readValue(hit.getSourceAsString(), Zkilloard.class));
            }

            BulkRequestBuilder bulkDelete =  esc.getClient().prepareBulk();
            for(Zkilloard kill:kills) {
                DeleteRequestBuilder del = new DeleteRequestBuilder(esc.getClient());
                del.setType("kills");
                del.setId(kill.getKillID()+"");
                del.setIndex("eve");

                bulkDelete.add(del);
            }
            bulkDelete.execute().actionGet();

            for(Zkilloard kill:kills){
                try {

                    esc.update(mapper.writeValueAsString(kill), "kills", kill.getKillID() + "");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();

                }
            }

            System.out.println(i*1000);

        }

        return Response.ok("done", MediaType.TEXT_PLAIN_TYPE).build();
    }


    @Path("broken")
    @GET
    public Response broken() throws JsonProcessingException {
        SearchRequestBuilder searchRequest = esc.getClient().prepareSearch(esc.getINDEX());

        searchRequest.setTypes("killmail");
        searchRequest.setQuery(QueryBuilders.matchAllQuery());
        searchRequest.setPostFilter(FilterBuilders.missingFilter("victim.shipType"));

        searchRequest.addAggregation(
                AggregationBuilders.terms("fuckYou").field("victim.allianceName.untouched").size(30)
        );
        SearchResponse searchResponse =searchRequest.execute().actionGet();
        Terms terms = searchResponse.getAggregations().get("fuckYou");
        ArrayList<Map<String,Long>> ret = new ArrayList<>();
        Map<String,Long> count = new HashMap<>();
        count.put("total",searchResponse.getHits().getTotalHits());
        ret.add(count);
        for(Terms.Bucket bucket :terms.getBuckets()){
            Map<String,Long> m = new HashMap<>();
            m.put(bucket.getKey(),bucket.getDocCount());
            ret.add(m);
        }
        return Response.ok(mapper.writeValueAsString(ret)).build();
    }


}
