package wtf.heck.rest.query;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by cerias on 06.02.15.
 */
@XmlRootElement
public class OverviewQuery {
    @XmlElement public String name;
    @XmlElement public Long from;
    @XmlElement public Long to;

    public OverviewQuery() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }
}
