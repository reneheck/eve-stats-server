package wtf.heck.rest.query;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by cerias on 05.02.15.
 */
@XmlRootElement
public class LossQuery {
    @XmlElement public Long from;
    @XmlElement public Long to;
    @XmlElement public long interval;
    @XmlElement public String name;

}
