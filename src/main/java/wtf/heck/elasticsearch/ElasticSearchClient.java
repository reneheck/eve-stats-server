package wtf.heck.elasticsearch;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

/**
 * Created by cerias on 07.06.14.
 */

@ApplicationScoped
public class ElasticSearchClient {

    Logger log = LoggerFactory.getLogger(ElasticSearchClient.class);



    private String INDEX  = "eve";
    Client client;

    Settings settings;

    ObjectMapper mapper = new ObjectMapper();

    public ElasticSearchClient(){
        log.error("ES Starting...");
        settings = ImmutableSettings.settingsBuilder()
                .put("cluster.name", "eve").build();
        this.client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress("localhost", 9300));

    }


    private void shutdown() {
        System.out.println("ES Stopping...");
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void insert(String json,String type,String id){


        IndexResponse response = client.prepareIndex(INDEX,type, id)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            log.info("Type: "+ type + " id: "+ id + ""+response.isCreated());

    }


    public void insert(String json,String type){


        IndexResponse response = client.prepareIndex(INDEX,type)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            log.info("Type: " + type + " id: " + response.getId() + "" + response.isCreated());

    }

    public void update(String json,String type,String id){

        DeleteResponse deleteResponse = client.prepareDelete(INDEX,type,id)
                .execute()
                .actionGet();

        IndexResponse response = client.prepareIndex(INDEX,type, id)
                .setSource(json)
                .execute()
                .actionGet();

        if(!response.isCreated())
            System.out.print(response);

    }



    public Client getClient(){
        return client;
    }

    public String getINDEX(){
        return INDEX;
    }

    public void bulkInsert(BulkRequestBuilder bulkRequest) {

        BulkResponse bulkResponse = bulkRequest.execute().actionGet();

        if (bulkResponse.hasFailures()) {
            log.error(bulkResponse.buildFailureMessage());
        }

    }




    /**
     *
     * @param bulkRequest
     * @param json
     * @param type
     * @param id
     * @return
     */
    public BulkRequestBuilder bulkEntry(BulkRequestBuilder bulkRequest, String json,String type,String id) {


        bulkRequest.add(client.prepareIndex(INDEX, type, id)
                        .setSource(json)
        );

        return bulkRequest;

    }




}
