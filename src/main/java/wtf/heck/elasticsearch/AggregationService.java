package wtf.heck.elasticsearch;

import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;

/**
 * Created by cerias on 04.04.15.
 */
@ApplicationScoped
public class AggregationService {


    public AggregationService() {

    }


    public ArrayList<AggregationBuilder> raceMarketAggregations(String type,String subType) {
        ArrayList<AggregationBuilder> aggregations = new ArrayList<>();

        aggregations.add(AggregationBuilders.filter("Minmatar").filter(FilterBuilders.termFilter(type + "."+subType+".marketGroups", "minmatar")));
        aggregations.add(AggregationBuilders.filter("Caldari").filter(FilterBuilders.termFilter(type + "."+subType+".marketGroups", "caldari")));
        aggregations.add(AggregationBuilders.filter("Gallente").filter(FilterBuilders.termFilter(type + "."+subType+".marketGroups", "gallente")));
        aggregations.add(AggregationBuilders.filter("Amarr").filter(FilterBuilders.termFilter(type + "."+subType+".marketGroups", "amarr")));
        aggregations.add(AggregationBuilders.filter("Pirate Faction").filter(FilterBuilders.termFilter(type+ "."+subType+".marketGroups", "pirate")));

        return aggregations;
    }

}
