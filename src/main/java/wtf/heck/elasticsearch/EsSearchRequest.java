package wtf.heck.elasticsearch;

/**
 * Created by cerias on 05.11.14.
 */
public class EsSearchRequest {

    EsSearch search;
    EsFilter filter;
    EsAgg agg;

    public EsSearchRequest(){

    }

    public EsSearch getSearch() {
        return search;
    }

    public void setSearch(EsSearch search) {
        this.search = search;
    }

    public EsFilter getFilter() {
        return filter;
    }

    public void setFilter(EsFilter filter) {
        this.filter = filter;
    }

    public EsAgg getAgg() {
        return agg;
    }

    public void setAgg(EsAgg agg) {
        this.agg = agg;
    }
}
