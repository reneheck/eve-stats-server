
package wtf.heck.zkillboard;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "characterID",
    "characterName",
    "corporationID",
    "corporationName",
    "allianceID",
    "allianceName",
    "factionID",
    "factionName",
    "securityStatus",
    "damageDone",
    "finalBlow",
    "weaponTypeID",
    "shipTypeID"
})
public class Attacker {

    @JsonProperty("characterID")
    private long characterID;
    @JsonProperty("characterName")
    private String characterName;
    @JsonProperty("corporationID")
    private long corporationID;
    @JsonProperty("corporationName")
    private String corporationName;
    @JsonProperty("allianceID")
    private long allianceID;
    @JsonProperty("allianceName")
    private String allianceName;
    @JsonProperty("factionID")
    private long factionID;
    @JsonProperty("factionName")
    private String factionName;
    @JsonProperty("securityStatus")
    private double securityStatus;
    @JsonProperty("damageDone")
    private double damageDone;
    @JsonProperty("finalBlow")
    private long finalBlow;
    @JsonProperty("weaponTypeID")
    private long weaponTypeID;
    @JsonProperty("shipTypeID")
    private long shipTypeID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("characterID")
    public long getCharacterID() {
        return characterID;
    }

    @JsonProperty("characterID")
    public void setCharacterID(long characterID) {
        this.characterID = characterID;
    }

    @JsonProperty("characterName")
    public String getCharacterName() {
        return characterName;
    }

    @JsonProperty("characterName")
    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    @JsonProperty("corporationID")
    public long getCorporationID() {
        return corporationID;
    }

    @JsonProperty("corporationID")
    public void setCorporationID(long corporationID) {
        this.corporationID = corporationID;
    }

    @JsonProperty("corporationName")
    public String getCorporationName() {
        return corporationName;
    }

    @JsonProperty("corporationName")
    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    @JsonProperty("allianceID")
    public long getAllianceID() {
        return allianceID;
    }

    @JsonProperty("allianceID")
    public void setAllianceID(long allianceID) {
        this.allianceID = allianceID;
    }

    @JsonProperty("allianceName")
    public String getAllianceName() {
        return allianceName;
    }

    @JsonProperty("allianceName")
    public void setAllianceName(String allianceName) {
        this.allianceName = allianceName;
    }

    @JsonProperty("factionID")
    public long getFactionID() {
        return factionID;
    }

    @JsonProperty("factionID")
    public void setFactionID(long factionID) {
        this.factionID = factionID;
    }

    @JsonProperty("factionName")
    public String getFactionName() {
        return factionName;
    }

    @JsonProperty("factionName")
    public void setFactionName(String factionName) {
        this.factionName = factionName;
    }

    @JsonProperty("securityStatus")
    public double getSecurityStatus() {
        return securityStatus;
    }

    @JsonProperty("securityStatus")
    public void setSecurityStatus(double securityStatus) {
        this.securityStatus = securityStatus;
    }

    @JsonProperty("damageDone")
    public double getDamageDone() {
        return damageDone;
    }

    @JsonProperty("damageDone")
    public void setDamageDone(double damageDone) {
        this.damageDone = damageDone;
    }

    @JsonProperty("finalBlow")
    public long getFinalBlow() {
        return finalBlow;
    }

    @JsonProperty("finalBlow")
    public void setFinalBlow(long finalBlow) {
        this.finalBlow = finalBlow;
    }

    @JsonProperty("weaponTypeID")
    public long getWeaponTypeID() {
        return weaponTypeID;
    }

    @JsonProperty("weaponTypeID")
    public void setWeaponTypeID(long weaponTypeID) {
        this.weaponTypeID = weaponTypeID;
    }

    @JsonProperty("shipTypeID")
    public long getShipTypeID() {
        return shipTypeID;
    }

    @JsonProperty("shipTypeID")
    public void setShipTypeID(long shipTypeID) {
        this.shipTypeID = shipTypeID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
