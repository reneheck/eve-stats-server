
package wtf.heck.zkillboard;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "typeID",
    "flag",
    "qtyDropped",
    "qtyDestroyed",
    "singleton"
})
public class Item {

    @JsonProperty("typeID")
    private long typeID;
    @JsonProperty("flag")
    private long flag;
    @JsonProperty("qtyDropped")
    private long qtyDropped;
    @JsonProperty("qtyDestroyed")
    private long qtyDestroyed;
    @JsonProperty("singleton")
    private String singleton;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("typeID")
    public long getTypeID() {
        return typeID;
    }

    @JsonProperty("typeID")
    public void setTypeID(long typeID) {
        this.typeID = typeID;
    }

    @JsonProperty("flag")
    public long getFlag() {
        return flag;
    }

    @JsonProperty("flag")
    public void setFlag(long flag) {
        this.flag = flag;
    }

    @JsonProperty("qtyDropped")
    public long getQtyDropped() {
        return qtyDropped;
    }

    @JsonProperty("qtyDropped")
    public void setQtyDropped(long qtyDropped) {
        this.qtyDropped = qtyDropped;
    }

    @JsonProperty("qtyDestroyed")
    public long getQtyDestroyed() {
        return qtyDestroyed;
    }

    @JsonProperty("qtyDestroyed")
    public void setQtyDestroyed(long qtyDestroyed) {
        this.qtyDestroyed = qtyDestroyed;
    }

    @JsonProperty("singleton")
    public String getSingleton() {
        return singleton;
    }

    @JsonProperty("singleton")
    public void setSingleton(String singleton) {
        this.singleton = singleton;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
